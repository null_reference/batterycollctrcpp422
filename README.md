# Battery Collector C++ 4.22
### EN
Battery Collector tutorial project updated to work correctly in Unreal Engine 4.22. Feel free to use this codebase along with the video tutorials.
### ES
Proyecto del tutorial Battery Collector actualizado para funcionar correctamente en Unreal Engine 4.22. Siéntanse libres de usar este codebase junto a los video tutoriales.


![alt text][screenshot]

[screenshot]: https://drive.google.com/uc?export=view&id=1ydRbU22X1SumObAT6v5-qEy3vo6xna9x "Project Screenshot from the Unreal Editor"

## Original tutorial
### EN
The original tutorial series is available on Youtube (click on image below)
### ES
La serie original de tutoriales está disponible en Youtube (click en la imagen de abajo):

[![](http://img.youtube.com/vi/mSRov77hNR4/0.jpg)](http://www.youtube.com/watch?v=mSRov77hNR4 "Battery Collector Video Tutorial Series")