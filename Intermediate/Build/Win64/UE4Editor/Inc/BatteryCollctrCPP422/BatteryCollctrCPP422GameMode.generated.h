// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
enum class EBatteryPlayState : uint8;
#ifdef BATTERYCOLLCTRCPP422_BatteryCollctrCPP422GameMode_generated_h
#error "BatteryCollctrCPP422GameMode.generated.h already included, missing '#pragma once' in BatteryCollctrCPP422GameMode.h"
#endif
#define BATTERYCOLLCTRCPP422_BatteryCollctrCPP422GameMode_generated_h

#define BatteryCollctrCPP422_Source_BatteryCollctrCPP422_BatteryCollctrCPP422GameMode_h_22_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetCurrentState) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(EBatteryPlayState*)Z_Param__Result=P_THIS->GetCurrentState(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetPowerToWin) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetPowerToWin(); \
		P_NATIVE_END; \
	}


#define BatteryCollctrCPP422_Source_BatteryCollctrCPP422_BatteryCollctrCPP422GameMode_h_22_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetCurrentState) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(EBatteryPlayState*)Z_Param__Result=P_THIS->GetCurrentState(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetPowerToWin) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetPowerToWin(); \
		P_NATIVE_END; \
	}


#define BatteryCollctrCPP422_Source_BatteryCollctrCPP422_BatteryCollctrCPP422GameMode_h_22_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesABatteryCollctrCPP422GameMode(); \
	friend struct Z_Construct_UClass_ABatteryCollctrCPP422GameMode_Statics; \
public: \
	DECLARE_CLASS(ABatteryCollctrCPP422GameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/BatteryCollctrCPP422"), BATTERYCOLLCTRCPP422_API) \
	DECLARE_SERIALIZER(ABatteryCollctrCPP422GameMode)


#define BatteryCollctrCPP422_Source_BatteryCollctrCPP422_BatteryCollctrCPP422GameMode_h_22_INCLASS \
private: \
	static void StaticRegisterNativesABatteryCollctrCPP422GameMode(); \
	friend struct Z_Construct_UClass_ABatteryCollctrCPP422GameMode_Statics; \
public: \
	DECLARE_CLASS(ABatteryCollctrCPP422GameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/BatteryCollctrCPP422"), BATTERYCOLLCTRCPP422_API) \
	DECLARE_SERIALIZER(ABatteryCollctrCPP422GameMode)


#define BatteryCollctrCPP422_Source_BatteryCollctrCPP422_BatteryCollctrCPP422GameMode_h_22_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	BATTERYCOLLCTRCPP422_API ABatteryCollctrCPP422GameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABatteryCollctrCPP422GameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(BATTERYCOLLCTRCPP422_API, ABatteryCollctrCPP422GameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABatteryCollctrCPP422GameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	BATTERYCOLLCTRCPP422_API ABatteryCollctrCPP422GameMode(ABatteryCollctrCPP422GameMode&&); \
	BATTERYCOLLCTRCPP422_API ABatteryCollctrCPP422GameMode(const ABatteryCollctrCPP422GameMode&); \
public:


#define BatteryCollctrCPP422_Source_BatteryCollctrCPP422_BatteryCollctrCPP422GameMode_h_22_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	BATTERYCOLLCTRCPP422_API ABatteryCollctrCPP422GameMode(ABatteryCollctrCPP422GameMode&&); \
	BATTERYCOLLCTRCPP422_API ABatteryCollctrCPP422GameMode(const ABatteryCollctrCPP422GameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(BATTERYCOLLCTRCPP422_API, ABatteryCollctrCPP422GameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABatteryCollctrCPP422GameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ABatteryCollctrCPP422GameMode)


#define BatteryCollctrCPP422_Source_BatteryCollctrCPP422_BatteryCollctrCPP422GameMode_h_22_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__DecayRate() { return STRUCT_OFFSET(ABatteryCollctrCPP422GameMode, DecayRate); } \
	FORCEINLINE static uint32 __PPO__PowerToWin() { return STRUCT_OFFSET(ABatteryCollctrCPP422GameMode, PowerToWin); } \
	FORCEINLINE static uint32 __PPO__HUDWidgetClass() { return STRUCT_OFFSET(ABatteryCollctrCPP422GameMode, HUDWidgetClass); } \
	FORCEINLINE static uint32 __PPO__CurrentWidget() { return STRUCT_OFFSET(ABatteryCollctrCPP422GameMode, CurrentWidget); }


#define BatteryCollctrCPP422_Source_BatteryCollctrCPP422_BatteryCollctrCPP422GameMode_h_19_PROLOG
#define BatteryCollctrCPP422_Source_BatteryCollctrCPP422_BatteryCollctrCPP422GameMode_h_22_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	BatteryCollctrCPP422_Source_BatteryCollctrCPP422_BatteryCollctrCPP422GameMode_h_22_PRIVATE_PROPERTY_OFFSET \
	BatteryCollctrCPP422_Source_BatteryCollctrCPP422_BatteryCollctrCPP422GameMode_h_22_RPC_WRAPPERS \
	BatteryCollctrCPP422_Source_BatteryCollctrCPP422_BatteryCollctrCPP422GameMode_h_22_INCLASS \
	BatteryCollctrCPP422_Source_BatteryCollctrCPP422_BatteryCollctrCPP422GameMode_h_22_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define BatteryCollctrCPP422_Source_BatteryCollctrCPP422_BatteryCollctrCPP422GameMode_h_22_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	BatteryCollctrCPP422_Source_BatteryCollctrCPP422_BatteryCollctrCPP422GameMode_h_22_PRIVATE_PROPERTY_OFFSET \
	BatteryCollctrCPP422_Source_BatteryCollctrCPP422_BatteryCollctrCPP422GameMode_h_22_RPC_WRAPPERS_NO_PURE_DECLS \
	BatteryCollctrCPP422_Source_BatteryCollctrCPP422_BatteryCollctrCPP422GameMode_h_22_INCLASS_NO_PURE_DECLS \
	BatteryCollctrCPP422_Source_BatteryCollctrCPP422_BatteryCollctrCPP422GameMode_h_22_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> BATTERYCOLLCTRCPP422_API UClass* StaticClass<class ABatteryCollctrCPP422GameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID BatteryCollctrCPP422_Source_BatteryCollctrCPP422_BatteryCollctrCPP422GameMode_h


#define FOREACH_ENUM_EBATTERYPLAYSTATE(op) \
	op(EBatteryPlayState::EPlaying) \
	op(EBatteryPlayState::EGameover) \
	op(EBatteryPlayState::EWon) \
	op(EBatteryPlayState::EUnknown) 

enum class EBatteryPlayState : uint8;
template<> BATTERYCOLLCTRCPP422_API UEnum* StaticEnum<EBatteryPlayState>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
