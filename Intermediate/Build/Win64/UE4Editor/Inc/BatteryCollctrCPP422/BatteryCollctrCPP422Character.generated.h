// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef BATTERYCOLLCTRCPP422_BatteryCollctrCPP422Character_generated_h
#error "BatteryCollctrCPP422Character.generated.h already included, missing '#pragma once' in BatteryCollctrCPP422Character.h"
#endif
#define BATTERYCOLLCTRCPP422_BatteryCollctrCPP422Character_generated_h

#define BatteryCollctrCPP422_Source_BatteryCollctrCPP422_BatteryCollctrCPP422Character_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execCollectPickups) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->CollectPickups(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execUpdatePower) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_PowerChange); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->UpdatePower(Z_Param_PowerChange); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetCurrentPower) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetCurrentPower(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetInitialPower) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetInitialPower(); \
		P_NATIVE_END; \
	}


#define BatteryCollctrCPP422_Source_BatteryCollctrCPP422_BatteryCollctrCPP422Character_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execCollectPickups) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->CollectPickups(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execUpdatePower) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_PowerChange); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->UpdatePower(Z_Param_PowerChange); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetCurrentPower) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetCurrentPower(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetInitialPower) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetInitialPower(); \
		P_NATIVE_END; \
	}


#define BatteryCollctrCPP422_Source_BatteryCollctrCPP422_BatteryCollctrCPP422Character_h_12_EVENT_PARMS
#define BatteryCollctrCPP422_Source_BatteryCollctrCPP422_BatteryCollctrCPP422Character_h_12_CALLBACK_WRAPPERS
#define BatteryCollctrCPP422_Source_BatteryCollctrCPP422_BatteryCollctrCPP422Character_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesABatteryCollctrCPP422Character(); \
	friend struct Z_Construct_UClass_ABatteryCollctrCPP422Character_Statics; \
public: \
	DECLARE_CLASS(ABatteryCollctrCPP422Character, ACharacter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/BatteryCollctrCPP422"), NO_API) \
	DECLARE_SERIALIZER(ABatteryCollctrCPP422Character)


#define BatteryCollctrCPP422_Source_BatteryCollctrCPP422_BatteryCollctrCPP422Character_h_12_INCLASS \
private: \
	static void StaticRegisterNativesABatteryCollctrCPP422Character(); \
	friend struct Z_Construct_UClass_ABatteryCollctrCPP422Character_Statics; \
public: \
	DECLARE_CLASS(ABatteryCollctrCPP422Character, ACharacter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/BatteryCollctrCPP422"), NO_API) \
	DECLARE_SERIALIZER(ABatteryCollctrCPP422Character)


#define BatteryCollctrCPP422_Source_BatteryCollctrCPP422_BatteryCollctrCPP422Character_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABatteryCollctrCPP422Character(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABatteryCollctrCPP422Character) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABatteryCollctrCPP422Character); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABatteryCollctrCPP422Character); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABatteryCollctrCPP422Character(ABatteryCollctrCPP422Character&&); \
	NO_API ABatteryCollctrCPP422Character(const ABatteryCollctrCPP422Character&); \
public:


#define BatteryCollctrCPP422_Source_BatteryCollctrCPP422_BatteryCollctrCPP422Character_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABatteryCollctrCPP422Character(ABatteryCollctrCPP422Character&&); \
	NO_API ABatteryCollctrCPP422Character(const ABatteryCollctrCPP422Character&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABatteryCollctrCPP422Character); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABatteryCollctrCPP422Character); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ABatteryCollctrCPP422Character)


#define BatteryCollctrCPP422_Source_BatteryCollctrCPP422_BatteryCollctrCPP422Character_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CameraBoom() { return STRUCT_OFFSET(ABatteryCollctrCPP422Character, CameraBoom); } \
	FORCEINLINE static uint32 __PPO__FollowCamera() { return STRUCT_OFFSET(ABatteryCollctrCPP422Character, FollowCamera); } \
	FORCEINLINE static uint32 __PPO__CollectionSphere() { return STRUCT_OFFSET(ABatteryCollctrCPP422Character, CollectionSphere); } \
	FORCEINLINE static uint32 __PPO__InitialPower() { return STRUCT_OFFSET(ABatteryCollctrCPP422Character, InitialPower); } \
	FORCEINLINE static uint32 __PPO__SpeedFactor() { return STRUCT_OFFSET(ABatteryCollctrCPP422Character, SpeedFactor); } \
	FORCEINLINE static uint32 __PPO__BaseSpeed() { return STRUCT_OFFSET(ABatteryCollctrCPP422Character, BaseSpeed); } \
	FORCEINLINE static uint32 __PPO__CharacterPower() { return STRUCT_OFFSET(ABatteryCollctrCPP422Character, CharacterPower); }


#define BatteryCollctrCPP422_Source_BatteryCollctrCPP422_BatteryCollctrCPP422Character_h_9_PROLOG \
	BatteryCollctrCPP422_Source_BatteryCollctrCPP422_BatteryCollctrCPP422Character_h_12_EVENT_PARMS


#define BatteryCollctrCPP422_Source_BatteryCollctrCPP422_BatteryCollctrCPP422Character_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	BatteryCollctrCPP422_Source_BatteryCollctrCPP422_BatteryCollctrCPP422Character_h_12_PRIVATE_PROPERTY_OFFSET \
	BatteryCollctrCPP422_Source_BatteryCollctrCPP422_BatteryCollctrCPP422Character_h_12_RPC_WRAPPERS \
	BatteryCollctrCPP422_Source_BatteryCollctrCPP422_BatteryCollctrCPP422Character_h_12_CALLBACK_WRAPPERS \
	BatteryCollctrCPP422_Source_BatteryCollctrCPP422_BatteryCollctrCPP422Character_h_12_INCLASS \
	BatteryCollctrCPP422_Source_BatteryCollctrCPP422_BatteryCollctrCPP422Character_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define BatteryCollctrCPP422_Source_BatteryCollctrCPP422_BatteryCollctrCPP422Character_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	BatteryCollctrCPP422_Source_BatteryCollctrCPP422_BatteryCollctrCPP422Character_h_12_PRIVATE_PROPERTY_OFFSET \
	BatteryCollctrCPP422_Source_BatteryCollctrCPP422_BatteryCollctrCPP422Character_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	BatteryCollctrCPP422_Source_BatteryCollctrCPP422_BatteryCollctrCPP422Character_h_12_CALLBACK_WRAPPERS \
	BatteryCollctrCPP422_Source_BatteryCollctrCPP422_BatteryCollctrCPP422Character_h_12_INCLASS_NO_PURE_DECLS \
	BatteryCollctrCPP422_Source_BatteryCollctrCPP422_BatteryCollctrCPP422Character_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> BATTERYCOLLCTRCPP422_API UClass* StaticClass<class ABatteryCollctrCPP422Character>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID BatteryCollctrCPP422_Source_BatteryCollctrCPP422_BatteryCollctrCPP422Character_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
